const fs = require("fs");
const asynch = require("async");
var stack = [];
//module one

const add = (a,b) =>{
    return a+b;
};

const sub = (a,b) =>{
    return a-b;
};

const mul = (a,b) =>{
    return a*b;
};

//module two asynch type
function asyn(){
fs.readFile('read.txt', 'utf-8',(err, data)=>{
    if(err)
    {
        return res.status(400).json({
            error:"not able to read"
        })
    }
    console.log(data);
});

console.log("This will come befor above statment in asynch");
}

//module three paraller async



var functionOne = function(callback) {
    callback('ERROR', null);
    

}

var functionTwo = function(callback) {
	callback(null, 'Second function result');
}

var functionThree = function(callback) {
	callback(null, 'Third function result');
}

stack.push(functionOne);
stack.push(functionTwo);
stack.push(functionThree);

async.parallel(stack, function(err, result) {
	return result;
});



//export module one, two and three

module.exports = {add, sub, mul, asyn, result};


