
const {add, sub, mul, asyn, result} = require('./modules');

//call module one
console.log(add(45,55));
console.log(sub(55,45));
console.log(mul(55*45));

//call module two
console.log(asyn);

//call module three
console.log(result);

